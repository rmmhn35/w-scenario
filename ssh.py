
import paramiko
 
def connectclient(host, port, username, password, keyfilepath, keyfiletype):
    """
    connectclient(host, port, username, password, keyfilepath, keyfiletype) -> connect_sshClient
 
    Creates a connect_ssh client connected to the supplied host on the supplied port authenticating as the user with
    supplied username and supplied password or with the private key in a file with the supplied path.
    If a private key is used for authentication, the type of the keyfile needs to be specified as DSA or RSA.
    :rtype: connect_sshClient object.
    """
    connect_ssh = None
    key = None
    transport = None
    try:
        if keyfilepath is not None:
            # Get private key used to authenticate user.
            if keyfiletype == 'DSA':
                # The private key is a DSA type key.
                key = paramiko.DSSKey.from_private_key_file(keyfilepath)
            else:
                # The private key is a RSA type key.
                key = paramiko.RSAKey.from_private_key(keyfilepath)
 
        # Create Transport object using supplied method of authentication.
        transport = paramiko.Transport((host, port))
        transport.connect(None, username, password, key)
         connect_ssh = paramiko.connect_sshClient.from_transport(transport)
        command = 'uptime' 
       (stdin, stdout, stderr) = s.exec_command(command)
       uptime_output = stdout
    except Exception as e:
        print('An error occurred creating connect_ssh client: %s' % ( e))
        if connect_ssh is not None:
            connect_ssh.close()
        if transport is not None:
            transport.close()
        pass
 